/*


 Source Schema         : patch 0.1,0.2 =>0.3
          Type         : beta
        Version        : 0.3
  File Encoding        : 65001

 Date: 30/03/2023 19:02:36
*/

-- ----------------------------
-- Records of mh_work_type
-- ----------------------------

DROP TABLE IF EXISTS `mh_work_type`;
CREATE TABLE `mh_work_type`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务类型',
  `deleted` tinyint(0) NULL DEFAULT NULL COMMENT '删除标识0:未删除,1:已删除',
  `create_user` bigint(0) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工作类型' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mh_hour_detail_sub
-- ----------------------------
DROP TABLE IF EXISTS `mh_hour_detail_sub`;
CREATE TABLE `mh_hour_detail_sub`  (
    `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `detail_id` bigint(0) NULL DEFAULT NULL COMMENT '详情Id',
    `work_type_id` bigint(0) NULL DEFAULT NULL COMMENT '工作类型Id',
    `use_hour` double(20, 2) NULL DEFAULT 0.00 COMMENT '工时',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工时填报详情表字表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mh_hour_detail_sub
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;


-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `wuyu_cost`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2076, '删除', 2071, 4, '', NULL, 1, 0, 'F', '0', '0', 'mh:project:work:remove', '#', 'admin', '2022-11-09 15:33:25', '', NULL, '');
INSERT INTO `wuyu_cost`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2075, '编辑', 2071, 3, '', NULL, 1, 0, 'F', '0', '0', 'mh:project:work:edit', '#', 'admin', '2022-11-09 15:31:25', '', NULL, '');
INSERT INTO `wuyu_cost`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2074, '新增', 2071, 2, '', NULL, 1, 0, 'F', '0', '0', 'mh:project:work:add', '#', 'admin', '2022-11-09 15:30:52', 'admin', '2022-11-09 15:30:59', '');
INSERT INTO `wuyu_cost`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2072, '查询', 2071, 1, '', NULL, 1, 0, 'F', '0', '0', 'mh:project:work:select', '#', 'admin', '2022-11-09 15:29:24', 'admin', '2022-11-09 15:32:36', '');
INSERT INTO `wuyu_cost`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2071, '业务类型', 2002, 5, 'workType', '/workingHours/workType', 1, 0, 'C', '0', '0', 'mh:project:work:select', 'nested', 'admin', '2022-11-09 15:27:17', 'admin', '2022-11-09 15:32:45', '');



ALTER TABLE `wuyu_cost`.`sys_project_user` 
ADD COLUMN `join_with_project` tinyint(255) NULL COMMENT '加入时间同项目开始时间，1:是,0:否' AFTER `update_time`,
ADD COLUMN `join_time` datetime NULL COMMENT '加入时间' AFTER `join_with_project`;

update sys_project_user set join_with_project =0 ,join_time = create_time;

-- ----------------------------
-- Table structure for sys_holiday
-- ----------------------------
DROP TABLE IF EXISTS `sys_holiday`;
CREATE TABLE `sys_holiday`  (
    `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `year` int(0) NULL DEFAULT NULL COMMENT '年份',
    `month` int(0) NULL DEFAULT NULL COMMENT '月份',
    `day` int(0) NULL DEFAULT NULL COMMENT '日',
    `date` date NULL DEFAULT NULL COMMENT '日期',
    `holiday` tinyint(0) NULL DEFAULT NULL COMMENT '是否法定节日期标识,1:是,0:否',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `sys_holiday_index_date`(`date`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节假日信息表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

