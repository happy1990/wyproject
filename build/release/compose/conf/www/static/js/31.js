(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[31],{

/***/ "./src/api/workingHour/projectManagement.js":
/*!**************************************************!*\
  !*** ./src/api/workingHour/projectManagement.js ***!
  \**************************************************/
/*! exports provided: getHourStatFill, getHourStatFillDetail, getCostProjectPosts, getCostProjectUsers, getProjectHourStatCalendar */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getHourStatFill\", function() { return getHourStatFill; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getHourStatFillDetail\", function() { return getHourStatFillDetail; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getCostProjectPosts\", function() { return getCostProjectPosts; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getCostProjectUsers\", function() { return getCostProjectUsers; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getProjectHourStatCalendar\", function() { return getProjectHourStatCalendar; });\n/* harmony import */ var _utils_request__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/utils/request */ \"./src/utils/request.js\");\n\n\n// 上报记录统计\nfunction getHourStatFill(date, id) {\n  return Object(_utils_request__WEBPACK_IMPORTED_MODULE_0__[\"default\"])({\n    url: \"/system/project/hour/stat/fill?localDate=\" + date + \"&projectId=\" + id,\n    method: \"get\"\n  });\n}\n\n// 项目工时统计详情\nfunction getHourStatFillDetail(params) {\n  return Object(_utils_request__WEBPACK_IMPORTED_MODULE_0__[\"default\"])({\n    url: \"/system/project/hour/stat/fill/detail\",\n    method: \"get\",\n    params: params\n  });\n}\n\n// 查询成本统计页面参与职位下拉框\nfunction getCostProjectPosts(projectId) {\n  return Object(_utils_request__WEBPACK_IMPORTED_MODULE_0__[\"default\"])({\n    url: \"/system/project/hour/project/posts?projectId=\" + projectId,\n    method: \"get\"\n  });\n}\n\n// 查询成本统计页面参与人员下拉框\nfunction getCostProjectUsers(projectId) {\n  return Object(_utils_request__WEBPACK_IMPORTED_MODULE_0__[\"default\"])({\n    url: \"/system/project/hour/project/users?projectId=\" + projectId,\n    method: \"get\"\n  });\n}\n//查询上报记录-日历\nfunction getProjectHourStatCalendar(date, id) {\n  return Object(_utils_request__WEBPACK_IMPORTED_MODULE_0__[\"default\"])({\n    url: '/system/project/hour/stat/calendar?localDate=' + date + \"&projectId=\" + id,\n    method: 'get'\n  });\n}\n\n//# sourceURL=webpack:///./src/api/workingHour/projectManagement.js?");

/***/ })

}]);